from flask import redirect, render_template, request, make_response
from app import app
from .forms import GameForm, LoginForm, RegisterForm, ChangeForm
from app import db, models
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import login_user, login_required, logout_user, current_user


@app.route('/')
def index():
    return redirect("allgames")


# Create game page
@app.route('/creategame', methods=['GET', 'POST'])
@login_required
def creategame():
    cookierequest = request.cookies.get('seen')
    form = GameForm()
    # Form data to create game entry
    if form.validate_on_submit():
        newgame = models.Game(developer=form.developer.data, title=form.title.data, description=form.description.data,
                        platform=form.platform.data)
        db.session.add(newgame)
        db.session.commit()
        return redirect("allgames")
    return render_template('addgame.html',
                           title='Create Game',
                           form=form,
                           seen=cookierequest)


# All game page
@app.route('/allgames', methods=['GET', 'POST'])
@login_required
def games():
    cookierequest = request.cookies.get('seen')
    # Finds all games user does not own
    currentUser = models.User.query.get(current_user.get_id())
    all_games = models.Game.query.all()
    my_games = currentUser.games
    unowned_games = []

    for games in all_games:
        if games not in my_games:
            unowned_games.append(games)

    # Post method to relate user to game in database
    if request.method == 'POST':
        t = currentUser
        gameId = request.form['submit']
        game = models.Game.query.get(gameId)
        game.owners.append(currentUser)

        db.session.add(game)
        db.session.commit()
        return redirect("/allgames")

    return render_template('allgames.html',
                           title='Game Library',
                           Game=unowned_games,
                           seen=cookierequest)


# Owned games page
@app.route('/ownedgames', methods=['GET', 'POST'])
@login_required
def owngame():
    cookierequest = request.cookies.get('seen')
    currentUser = models.User.query.get(current_user.get_id())
    my_games = currentUser.games
    # Post method to remove users ownership of game
    if request.method == 'POST':
        gameId = request.form['submit']
        game = models.Game.query.get(gameId)
        game.owners.remove(currentUser)

        db.session.add(game)
        db.session.commit()
        return redirect("/ownedgames")
    return render_template('ownedgames.html',
                           title='Owned Games',
                           Game=my_games,
                           seen=cookierequest)


# Login page
@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    # Form to take user login details, checks password entered with hashed password and logs user in
    if form.validate_on_submit():
        user = models.User.query.filter_by(username=form.username.data).first()
        if user:
            if check_password_hash(user.password, form.password.data):
                # Log for successful login
                titleforlog = user.username + " Login Successful!"
                messageforlog = user.username + " has been succesfully logged in."
                log = models.Logging(Title=titleforlog, Severity=0, Message=messageforlog)
                db.session.add(log)
                db.session.commit()

                login_user(user, remember=form.remember.data)
                return redirect('/')
        # Log for invalid user
        titleforlog = form.username.data + " Login Attempt Denied"
        messageforlog = "Invalid username or password for " + form.username.data + " has been entered!"
        log = models.Logging(Title=titleforlog, Severity=2, Message=messageforlog)
        db.session.add(log)
        db.session.commit()

        return render_template('login.html',
                               title='Login',
                               form=form,
                               passwords_invalid=True)
    return render_template('login.html',
                           title='Login',
                           form=form)


# Logout method
@app.route('/logout')
@login_required
def logout():
    # Log for successful logout
    titleforlog = "Logout Successful!"
    messageforlog = "The user has successfully logged out"
    log = models.Logging(Title=titleforlog, Severity=0, Message=messageforlog)
    db.session.add(log)
    db.session.commit()

    logout_user()
    return redirect("/login")


# Register page
@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm()
    # Form to take user register data, adds user to database and hashes password with sha256
    if form.validate_on_submit():
        alreadyuser = models.User.query.filter_by(username=form.username.data)
        if (models.User.query.filter_by(username=form.username.data).first() is None):
            if form.repassword.data == form.password.data:
                # Hashing the password
                hashed_password = generate_password_hash(form.password.data, method='sha256')
                new_user = models.User(username=form.username.data, email=form.email.data, password=hashed_password)
                db.session.add(new_user)
                db.session.commit()
                return redirect("/login")
            else:
                # Log for passwords not matching
                titleforlog = form.username.data + " Register Attempt Failed"
                messageforlog = "Passwords for " + form.username.data + " did not match!"
                log = models.Logging(Title=titleforlog, Severity=1, Message=messageforlog)
                db.session.add(log)
                db.session.commit()

                return render_template('register.html',
                                       title='Register',
                                       form=form,
                                       passwords_invalid=True)

        return render_template('register.html',
                        title='Register',
                        form=form,
                        user_exists=True)
    return render_template('register.html',
                           title='Register',
                           form=form)


# Change password page
@app.route('/changepassword', methods=['GET', 'POST'])
def changepassword():
    form = ChangeForm()
    # Form to take users change password data and change the password stored in the database
    if form.validate_on_submit():
        user = models.User.query.filter_by(username=form.usersname.data).first()
        if (form.oldpassword.data != form.newpassword.data):
            if user:
                if check_password_hash(user.password, form.oldpassword.data):
                    # Hashing the password
                    hashed_password = generate_password_hash(form.newpassword.data, method='sha256')

                    user.password = hashed_password
                    db.session.commit()
                    return redirect("/login")
                else:
                    # Log for invalid password
                    titleforlog = form.usersname.data + " Change Password Attempt Denied"
                    messageforlog = "Invalid password for " + user.username + " has been entered!"
                    log = models.Logging(Title=titleforlog, Severity=2, Message=messageforlog)
                    db.session.add(log)
                    db.session.commit()

                    return render_template('changepassword.html',
                                           title='Change Password',
                                           form=form,
                                           passwords_invalid=True)
            else:
                # Log for invalid user
                titleforlog = form.usersname.data + " Change Password Attempt Denied"
                messageforlog = "Invalid user " + form.usersname.data + " been entered!"
                log = models.Logging(Title=titleforlog, Severity=2, Message=messageforlog)
                db.session.add(log)
                db.session.commit()

                return render_template('changepassword.html',
                                       title='Change Password',
                                       form=form,
                                       passwords_invalid=True)
        else:
            # Log for the same password being entered
            titleforlog = form.usersname.data + " Change Password Attempt Denied"
            messageforlog = "Password entered is the same as old password!"
            log = models.Logging(Title=titleforlog, Severity=2, Message=messageforlog)
            db.session.add(log)
            db.session.commit()

            return render_template('changepassword.html',
                                   title='Change Password',
                                   form=form,
                                   passwords_invalid=True)
    return render_template('changepassword.html',
                           title='Change Password',
                           form=form)


# Sets the cookie policy seen cookie
@app.route("/setcookie")
def setcookie():
    # Log for user acceping cookie policy
    titleforlog = "Cookie Policy"
    messageforlog = "The user accepted the cookie policy"
    log = models.Logging(Title=titleforlog, Severity=0, Message=messageforlog)
    db.session.add(log)
    db.session.commit()

    response = make_response(redirect('/allgames'))
    response.set_cookie('seen', 'True')
    return response


# Gets value of cookie policy seen cookie
@app.route("/getcookie")
def getcookie():
    cookierequest = request.cookies.get('seen')
    return cookierequest


# Location page
@app.route('/location', methods=['GET', 'POST'])
@login_required
def location():
    cookierequest = request.cookies.get('seen')
    return render_template('Location.html',
                           title='Location',
                           seen=cookierequest)
