from flask_wtf import Form, FlaskForm
from wtforms import TextAreaField, TextField, PasswordField, BooleanField
from wtforms.validators import DataRequired, Length, Email


# Form to acquire user login data
class LoginForm(FlaskForm):
    username = TextField('username', validators=[DataRequired(), Length(min=4, max=15)])
    password = PasswordField('password', validators=[DataRequired(), Length(min=8, max=80)])
    remember = BooleanField('remember me')


# Form to acquire register data
class RegisterForm(Form):
    username = TextField('username', validators=[DataRequired(), Length(min=4, max=15)])
    email = TextField('email', validators=[DataRequired(), Email(message='Invalid email'), Length(max=50)])
    password = PasswordField('password', validators=[DataRequired(), Length(min=8, max=80)])
    repassword = PasswordField('repassword', validators=[DataRequired(), Length(min=8, max=80)])


# Form to acquire game data
class GameForm(Form):
    title = TextField('game', validators=[DataRequired(), Length(min=2, max=40)])
    developer = TextField('developer', validators=[DataRequired(), Length(min=8, max=40)])
    description = TextAreaField('description', validators=[DataRequired(), Length(min=2, max=40)])
    platform = TextField('platform', validators=[DataRequired(), Length(min=8, max=25)])

# Form to change password
class ChangeForm(Form):
    usersname = TextField('usersname', validators=[DataRequired(), Length(min=4, max=15)])
    oldpassword = PasswordField('oldpassword', validators=[DataRequired(), Length(min=8, max=80)])
    newpassword = PasswordField('newpassword', validators=[DataRequired(), Length(min=8, max=80)])