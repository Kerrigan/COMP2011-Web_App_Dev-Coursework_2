from app import db, login_manager


# Login manager to handle user logins
@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


# Library relation table linking user to game to store users owned games
library = db.Table('library', db.Model.metadata,
                   db.Column('user_id', db.Integer, db.ForeignKey('user.id'), primary_key=True),
                   db.Column('game_id', db.Integer, db.ForeignKey('game.id'), primary_key=True),
                   db.PrimaryKeyConstraint('game_id', 'user_id')
                   )


# User model holding user data, unique id, email, username, password and a relationship to the games they own
# RETURNS: get_id, is_active and is_authenticated
class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(50), unique=True)
    username = db.Column(db.String(15), unique=True)
    password = db.Column(db.String(80))
    games = db.relationship('Game', secondary=library, backref=db.backref('owners', lazy='dynamic'))

    def get_id(self):
        return unicode(self.id)

    def is_active(self):
        return True

    def is_authenticated(self):
        return True


# Game model holding game data, unique id, developer details, title, description(Genre) and platform
class Game(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    developer = db.Column(db.String(40))
    title = db.Column(db.String(40))
    description = db.Column(db.String(40))
    platform = db.Column(db.String(25))

# Logging model holding log information, title, severity of log and message describing log
class Logging(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    Title = db.Column(db.String(50))
    Severity = db.Column(db.Integer)
    Message = db.Column(db.String)