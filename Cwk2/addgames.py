from app import db, models
x=[]

with open("game.csv", "rt") as filestream:
    for line in filestream.readlines():
        y = line.strip().split(",")
        p = models.Game(developer=y[0],title=y[1], description=y[2],platform=y[3])
        db.session.add(p)
    db.session.commit()
