import os
import unittest
from app import app, models, db
from sqlite3 import IntegrityError


class TestCase(unittest.TestCase):

    # Testing setup
    def setUp(self):

        # Clear the database
        db.session.close()
        db.drop_all()

        # Reinitialising the database
        db.create_all()

    # Test user creation
    def test_user_creation(self):
        testuser = models.User(username="testuser", email="test@tester.com", password="thisisjustatest")
        db.session.add(testuser)
        db.session.commit()
        users = models.User.query.all()
        assert(testuser in users)

    # Test same user
    def test_no_same_username(self):
        firstuser = models.User(username="sameuser", email="user1@user.com", password="testpassword1")
        db.session.add(firstuser)
        db.session.commit()
        seconduser = models.User(username="sameuser", email="user2@user.com", password="testpassword2")

        self.assertRaises(IntegrityError, db.session.add(seconduser))

    # Test same email
    def test_no_same_email(self):
        firstuser = models.User(username="user1", email="test@user.com", password="testpassword1")
        db.session.add(firstuser)
        db.session.commit()
        seconduser = models.User(username="user2", email="test@user.com", password="testpassword2")

        self.assertRaises(IntegrityError, db.session.add(seconduser))

    # Test too long email
    def test_email_boundary(self):
        firstuser = models.User(username="user4", email="thisuserwillworkasemailis80charactersbutnotoverastheboundaryis80wearehe@user.com", password="testpassword2")
        db.session.add(firstuser)
        db.session.commit()
        seconduser = models.User(username="user3", email="thisemailissupposedtobetoolongtotestboundarycasewhichissettotake80characters@user.com", password="testpassword1")

        self.assertRaises(IntegrityError, db.session.add(seconduser))

    # Test too long username
    def test_username_boundary(self):
        firstuser = models.User(username="usernameworks15", email="test@test.com", password="testpassword2")
        db.session.add(firstuser)
        db.session.commit()
        seconduser = models.User(username="usernamedoesnotworkasitistoolong", email="test2@test.com", password="testpassword1")

        self.assertRaises(IntegrityError, db.session.add(seconduser))

    # Test too long password
    def test_password_boundary(self):
        firstuser = models.User(username="username15", email="test1@test.com", password="thispasswordwillworkcauseitis80characterslongthisisjusttotestitisworkingwith80ch")
        db.session.add(firstuser)
        db.session.commit()
        seconduser = models.User(username="username12", email="test3@test.com", password="thispasswordwillnotworkcauseitis81characterslongichosethislengthassomepeoplelikes")

        self.assertRaises(IntegrityError, db.session.add(seconduser))

    # Test Game creation
    def test_developer_boundary(self):
        game1 = models.Game(developer="Test Dev", title="Test Title", description="Test Desc", platform="Test Plat")
        db.session.add(game1)
        db.session.commit()

        games = models.Game.query.all()
        assert(game1 in games)

    # Test too long developer
    def test_developer_boundary(self):
        game1 = models.Game(developer="thedeveloperboundryis40sothisokasitis40!", title="testtitle", description="testdesc", platform="platform")
        db.session.add(game1)
        db.session.commit()
        game2 = models.Game(developer="thiswillfailandsendanIntegrityErrorasit41", title="testtitle", description="testdesc", platform="platform")

        self.assertRaises(IntegrityError, db.session.add(game2))

    # Test too long title
    def test_title_boundary(self):
        game1 = models.Game(developer="testdev", title="thetitleboundryis40sothisokasitis40!now!", description="testdesc", platform="platform")
        db.session.add(game1)
        db.session.commit()
        game2 = models.Game(developer="testdev", title="thiswillfailandsendanIntegrityErrorasit41", description="testdesc", platform="platform")

        self.assertRaises(IntegrityError, db.session.add(game2))

    # Test too long description
    def test_description_boundary(self):
        game1 = models.Game(developer="testdev", title="testtitle", description="thedescriptionboundryis40sothisokasitis40!", platform="platform")
        db.session.add(game1)
        db.session.commit()
        game2 = models.Game(developer="testdev", title="testtitle", description="thiswillfailandsendanIntegrityErrorasit41", platform="platform")

        self.assertRaises(IntegrityError, db.session.add(game2))

    # Test too long platform
    def test_platform_boundary(self):
        game1 = models.Game(developer="testdev", title="testtitle", description="thetitleboundryis40sothisokasitis40!", platform="platform")
        db.session.add(game1)
        db.session.commit()
        game2 = models.Game(developer="testdev", title="testtitle", description="thiswillfailandsendanIntegrityErrorasit41", platform="platform")

        self.assertRaises(IntegrityError, db.session.add(game2))


    # Test too long log_Title
    def test_log_boundary(self):
        game1 = models.Logging(Title="thistitleisokayasitis50characterslongwhichisthebou", Severity=0, Message="testmessage")
        db.session.add(game1)
        db.session.commit()
        game2 = models.Logging(Title="thistitleisnotokayasitisgoingover50charactersoanIntegrityError", Severity=0, Message="testmessage")

        self.assertRaises(IntegrityError, db.session.add(game2))

    # Testing cleanup
    def tearDown(self):
        db.session.remove()
        db.drop_all()
        db.create_all()


if __name__ == '__main__':
    unittest.main()
